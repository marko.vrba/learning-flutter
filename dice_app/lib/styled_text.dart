import 'package:flutter/material.dart';

class StyledText extends StatelessWidget
{
  StyledText(this.inputText, {super.key});

  final String inputText;

  @override
  Widget build(BuildContext context) {
    return Text(inputText, style: const TextStyle(
        color: Colors.white,
        fontSize: 40
    ),);
  }

}