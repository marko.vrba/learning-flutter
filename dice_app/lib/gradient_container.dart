import 'package:flutter/material.dart';

import 'dice.dart';

const topLeftAlignment = Alignment.topLeft;
const bottomRightAlignment = Alignment.bottomRight;

class GradientContainer extends StatelessWidget {
  const GradientContainer({super.key, required this.colors});

  final List<Color> colors;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: colors,
              begin: topLeftAlignment,
              end: bottomRightAlignment
          )
      ),
      child: const Center(
        child: Dice(),
      ),
    );
  }
}