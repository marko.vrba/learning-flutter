import 'package:flutter/material.dart';
import 'gradient_container.dart';
    
    void main() {

      final List<Color> colors = [Colors.amber, Colors.deepOrange];

      runApp(
          MaterialApp(
          home: Scaffold(
            body: GradientContainer(colors: colors)
          ),
        )
      );
    }