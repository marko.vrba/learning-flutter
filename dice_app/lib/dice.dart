import 'dart:math';

import 'package:flutter/material.dart';

final randomizer = Random();

class Dice extends StatefulWidget {
  const Dice({super.key});

  @override
  State<Dice> createState() => _DiceState();
}

class _DiceState extends State<Dice> {

  String activeDice = "assets/images/dice-1.png";

  void rollDice() {
    int number = randomizer.nextInt(6) + 1;
    setState(() {
      activeDice = "assets/images/dice-$number.png";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(activeDice),
        SizedBox(height: 20,),
        OutlinedButton(
            onPressed: rollDice,
            style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                textStyle: TextStyle(fontSize: 28)),
            child: Text("Roll the dice")),
        ElevatedButton(
            onPressed: rollDice,
            style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                textStyle: TextStyle(fontSize: 28)),
            child: Text("Roll the dice")),
        TextButton(
            onPressed: rollDice,
            style: TextButton.styleFrom(
                foregroundColor: Colors.white,
                textStyle: TextStyle(fontSize: 28)),
            child: Text("Roll the dice")),
      ],
    );
  }
}
